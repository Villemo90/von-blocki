<!doctype html>
<html>
<?php include 'html/includes/head.php';?>
<body>

        <div id="content">
          <header>
            <?php include 'html/includes/menu.php';?>
          </header>
          <span class="slide">
            <a href="#" onclick="openSlideMenu()">
              <i class="fas fa-bars"></i>
            </a>
          </span>
          <div class="contact_text">
            <span>HALO</span>
            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h3>
          </div>
            <form role="form">
              <div class="row">
                <div class="form-group col-lg-6">
                  <label for="name">name</label>
                  <input type="text" placeholder="What is your name?" class="form-control" autocomplete="off">
                </div>
                <div class="form-group col-lg-6">
                  <label for="email">e-mail</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Your email address please..." autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="title">title</label>
                    <input type="text" placeholder="I'm listening..." class="form-control" autocomplete="off">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="form-group col-lg-12">
                    <label for="message">message</label>
                    <textarea class="form-control" class="form-control" rows="6" autocomplete="off"></textarea>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="send_button angrytext" align="center">
                  <button type="submit"><p>Send</p></button>
                </div>
              </div>
            </form>
          <footer>
            <?php include 'html/includes/footer.php';?>
          </footer>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
