<!doctype html>
<html>
<?php include 'html/includes/head.php';?>
<body>

        <div id="content">
          <header>
            <?php include 'html/includes/menu.php';?>
          </header>
          <span class="slide">
            <a href="#" onclick="openSlideMenu()">
              <i class="fas fa-bars"></i>
            </a>
          </span>
          <div class="about_me">
          <div class="portraits_home">
            <img src="vonblocki.png" alt="vonblocki" onmouseover="this.src='vonblocki2.png'" onmouseout="this.src='vonblocki.png'"/>
          </div>
          <div class="introduction_text">
            <p>Malen ist eine Leidenschaft - genauso intensiv und genauso schwierig. Lust</p>
            <p>und Last gleichzeitig.Auf jeden Fall eine Herausforderung.Wer Schreibt, kennt die Angst vorm</p>
            <p>ersten Satz. Wer malt,fühlt die Angst vorm ersten Pinselstrich auf einer leeren</p>
            <p>Leinward. Doch ich will mich ausdrücken,ausprobieren. Grenzen erfharen und</p>
            <p>überschreiten. Nicht mit Worten, sondern mit Bildern.Ich liebe es, große Flächen</p>
            <p>verführerisch bunt zu gestalten. So bunt, wie unsere Welt selten ist.Kräftige Farben</p>
            <p>gehören zu meinen Fantasiewelten im Pop-Art_Still
              <span><img class="signature_img" src="content/images/signature.jpg" alt="signature"></span>
            </p>
          </div>
        </div>
          <footer>
            <?php include 'html/includes/footer.php';?>
          </footer>
        </div>

      </div>
    </div>
  </div>
</body>
</html>
